<?php
/**
 * Created by PhpStorm.
 * User: mobileapps
 * Date: 2018-12-04
 * Time: 10:24
 */

/**
 * ============================================================================
 * HTTP Status Codes
 * ============================================================================
 */

/**
 * 1xx Informational
 */

define('HTTP_CODE_100', '100 - CONTINUE');

/**
 * 2xx Success
 */

define('HTTP_CODE_200', '200 - OK');
define('HTTP_CODE_201', '201 - CREATED');
define('HTTP_CODE_202', '202 - ACCEPTED');
define('HTTP_CODE_203', '203 - NON-AUTHORITATIVE INFORMATION');
define('HTTP_CODE_204', '204 - NO CONTENT');
define('HTTP_CODE_205', '205 - RESET CONTENT');
define('HTTP_CODE_206', '206 - PARTIAL CONTENT');
define('HTTP_CODE_226', '226 - IM USED');

/**
 * 3xx Redirection
 */

define('HTTP_CODE_304', '304 - Not Modified');

/**
 * 4xx Client Error
 */

define('HTTP_CODE_400', '400 - BAD REQUEST');
define('HTTP_CODE_401', '401 - UNAUTHORIZED');
define('HTTP_CODE_403', '403 - FORBIDDEN');
define('HTTP_CODE_404', '404 - NOT FOUND');
define('HTTP_CODE_409', '409 - CONFLICT');

/**
 * 5xx Server Error
 */

define('HTTP_CODE_500', '500 - INTERNAL SERVER ERROR');


/**
 * ============================================================================
 * Warning & Error Messages
 * ============================================================================
 */

define('MSG_ERROR_DB', 'Database Error');
define('MSG_ERROR_SQL', 'SQL Error');
define('MSG_ERROR_QUERY', 'SQL Query');
define('MSG_ERROR_AUTH', 'Authentication missing or invalid');
define('MSG_ERROR_EMAIL_IN_USE', 'Email address is already in use');
define('MSG_ERROR_NON_EXISTED_RECORD', 'Unable to update non-existing record');
