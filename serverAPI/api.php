<?php

require_once 'vendor/autoload.php';
require_once 'includes/constant.php';
require_once 'includes/message.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


//make a change on here;
if (Local_DB) {
    DB::$dbName = 'cookingapp';
    DB::$user = 'cookingapp';
    DB::$password = "q1xRoebAputWsPem";
    DB::$host = '127.0.0.1'; // localhost'; // try 127.0.0.1 if doesn't work on mac
    DB::$port = 3333;
} else {
    DB::$dbName = DB_NAME_AWS;
    DB::$user = DB_USER_AWS;
    DB::$password = DB_PASS_AWS;
    DB::$host = DB_HOST_AWS;
    DB::$port = DB_PORT_AWS;
}

DB::$encoding = DB_CODE_AWS;

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';

function error_handler($params)
{
    global $log;
    if (isset($params['query'])) {
        $log->error("SQL Error: " . $params['error']);
        $log->error("SQL Query: " . $params['query']);
    } else {
        $log->error("Database Error: " . $params['error']);
    }
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error');
    die; // don't want to keep going if a query broke
}

$app = new \Slim\Slim();
// this script will always return JSON to any client

//GET from /recipes
$app->get('/recipes', function () use ($app, $log) {
    $recipeList = DB::query("SELECT `id`, `ownerid`, `name`, `description`, `category`, `preparationTime`, `numberofservings`,`pictureid`, `ingredients` FROM `recipes`");
    //unset($itemsList['picture']); // do NOT send picture
    echo json_encode($recipeList, JSON_PRETTY_PRINT);
});

//GET from /recipes/:id
$app->get('/recipes/:id', function ($id) use ($app) {
    $recipe = DB::queryFirstRow("SELECT `id`, `ownerid`, `name`, `description`, `category`, `preparationTime`, `numberofservings`,`pictureid`, `ingredients` FROM recipes WHERE id=%i", $id);
    if ($recipe) {
        //unset($recipe['picture']); // do NOT send picture
        echo json_encode($recipe, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});


//get recipes by owner id --- by Le Hu
$app->get('/:ownerid/recipes', function ($owner) use ($app) {
    $recipe = DB::query("select id,name,description,category,pictureid from recipes where ownerid=%i", $owner);
    if ($recipe) {
        echo json_encode($recipe, JSON_PRETTY_PRINT);
    } else {
        // $new= array();
        // echo json_encode($new,JSON_PRETTY_PRINT);
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

//******************************
//******************************
//*******************STEPS
$app->get('/steps/:recipeid', function ($recipeid) use ($app) {
    $stepList = DB::query("SELECT `stepnumber`, `step` FROM steps WHERE recipeid=%i", $recipeid);
    echo json_encode($stepList, JSON_PRETTY_PRINT);

});


//GET from /steps /:recipeID  recipes WHERE recipeID=%i", $recipeID
$app->get('/steps', function () use ($app, $log) {
    $stepsList = DB::query("SELECT * FROM steps");
    echo json_encode($stepsList, JSON_PRETTY_PRINT);
});

//GET from /steps/:recipeid
$app->get('/steps/:recipeid', function ($recipeid) use ($app) {
    $stepList = DB::query("SELECT `stepnumber`, `step` FROM steps WHERE recipeid=%i", $recipeid);

    echo json_encode($stepList, JSON_PRETTY_PRINT);

});

$app->post('/step', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    DB::insert('steps', $data);
    $id = DB::insertId();
    echo json_encode($id);
});

//******************************
//******************************
//*******************INGREDIENTS

//GET ingredients with recipeid
$app->get('/ingredients/:recipeID', function ($recipeid) use ($app, $log) {
    $ingredientsList = DB::query('SELECT ingredients FROM ingredients WHERE recipeID = %i', $recipeid);
    echo json_encode($ingredientsList, JSON_PRETTY_PRINT);
});

//GET ingredients with recipeid
$app->post('/ingredients', function () use ($app, $log) {
    $json = $app->request()->getBody();
    $data = json_decode($json, true);
    DB::insert('ingredients', $data);
    $id = DB::insertId();
    echo json_encode($id);
});




//******************************
//******************************
//*******************PICTURES

//POST of /recipe without picture
$app->post('/recipes', function () use ($app) {
    $json = $app->request()->getBody();
   // unset($json['picture']); // do NOT send picture
    $data = json_decode($json, true);
    // FIXME: verify user data is valid

    DB::insert('recipes', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});

$app->post('/recipes/:id/picture', function ($id) use ($app, $log) {
    $binary = $app->request()->getBody();
    DB::update('recipes', array('picture' => $binary), 'id=%d', $id);
    echo json_encode(true);
});

$app->get('/recipes/:id', function ($id) use ($app, $log) {
    $binary = DB::queryFirstField("SELECT picture FROM pictures WHERE id=%i", $id);
    $app->response()->header('content-type', 'image/jpeg');
    echo $binary;
});


//PUT image for recipe

$app->put('/recipes/:id/picture', function ($id) use ($app) {

    $json = $app->request()->getBody();
    $data = json_decode($json, true);

    DB::update('recipes', $data, 'id=%d AND picture=%d', $id, $data);
    echo json_encode(true);
});


//fort get  picturn;
$app->get('/pictures/:id', function ($id) use ($app) {
    $picture = DB::queryFirstRow('select id, picture from pictures where id=%i', $id);
//    $app->response()->header('content-type', 'image/jpeg');
    $picture['picture'] = base64_encode($picture['picture']);
//    echo $picture;
    echo json_encode($picture, JSON_PRETTY_PRINT);
});

$app->post('/uploadpic/', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);

    $data['picture'] = base64_decode($data['picture']);
    DB::insert('pictures', $data);
    $id = DB::insertId();
    echo json_encode($id, JSON_PRETTY_PRINT);
});

//POST /recipes    - send all data except for picture***done
//
//GET /recipes - return all data EXCEPT for picture ****done
//GET /recipes/:id - return all data of one record EXCEPT for picture ****done 
//
//GET /recipes/:id:/picture - return picture only, binary data
//PUT /recipes/:id/picture - update picture field only, send binary data, not json ****TODO


/*-----------------------------*/
// Le HU
// [start access user ]; before you call these api pleas take note,
// you should have finished validate on client side;
/**
 * Access to user table; and get user info; includes login,register.
 * and binding with google id;
 */

//$app->post('/user/', function () use ($log, $app) {
//    $json = $app->request->getBody();
//    $data = json_encode($json, true);
//    //todo: do I need check user info?
//    DB::insert('users', $data);
//    $app->response()->status(HTTP_CODE_201);
//
//});

// * get all users, only be used to test api;

$app->get('/users/', function () {
    $user_list = DB::query("select * from users");
    echo json_encode($user_list, JSON_PRETTY_PRINT);
});


/**
 * return user info by correct email account;
 */
$app->get('/user/:email', function ($email) {
    $user = DB::query("select * from users where email=%s", $email);
    if ($user) {
        echo json_encode($user, JSON_PRETTY_PRINT);
    } else {
        echo json_encode(HTTP_CODE_404);
    }
});

/**
 * register a new user, insert a new user;
 */
$app->post('/register', function () use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, true);
    //check email is used or not;
    $isUsed = DB::queryFirstRow("select * from users where email=%s", $data['email']);
//    var_dump($isUsed);
    if (count($isUsed) > 0) {
        echo json_encode("-1");
    } else {
        DB::insert('users', $data);
        $app->response()->status(HTTP_CODE_201);
        $id = DB::insertId();
        echo json_encode($id);
    }
});

/**
 * update user profile;
 */
$app->put('/update/:id', function ($id) use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    DB::update('users', $data, 'id=%d', $id);
    echo json_encode(true);

});

// [End of finishing user management api]
// favorites table

$app->get('/favorite/:ownerId', function ($ownerId) use ($app) {
    $recipe = DB::query("select recipes.id as id,name,description,category,pictureid from recipes 
    inner join favorites on favorites.recipeid = recipes.id  where favorites.userId=%i", $ownerId);
    if ($recipe) {
        echo json_encode($recipe, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

$app->post('/addfavorite', function () use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, true);
    $resutl= DB::delete('favorites','userid=%i',$data['userId'],'recipeId=%i',$data['recipeId']);
    // echo json_encode($resutl);
    DB::insert('favorites', $data);
    $id = DB::insertId();
    echo json_encode($id);
});

$app->delete('/removefavorite/:userid/:recipeId',function($userId,$recipeId){
    DB::delete('favorites', 'userid=%i',$userId, 'recipeId=%i',$recipeId);
    echo json_encode(DB::affectedRows() != 0);
});

$app->get('/isfavorite/:userId/:recipeId',function($userId,$recipeId){
    $data=DB::query('select * from favorites where userid=%i and recipeid=%i',$userId,$recipeId);

    if($data){
        echo json_encode(true);
    }else{
        echo json_encode(false);
    }
});

$app->run();

