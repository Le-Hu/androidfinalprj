package com.example.mobileapps.myrecipebook;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.services.RecipeService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    private RecyclerView rvRecipes;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter rvAdapter;

    private List<Recipes> recipesList;


    public HomeFragment() {
        // Required empty public constructor
    }

    public List<Recipes> loadRecipes() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:7777/api.php/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RecipeService recipeService = retrofit.create(RecipeService.class);

        Call<List<Recipes>> call = recipeService.getRecipe();

        call.enqueue(new Callback<List<Recipes>>() {
            @Override
            public void onResponse(Call<List<Recipes>> call, Response<List<Recipes>> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Code " + response.code());
                }
                List<Recipes> newList = response.body();
                Log.v(TAG, "response.body:  " + response.body());

//                Toast.makeText(getContext(), "succeed", Toast.LENGTH_LONG).show();

                recipesList.clear();
                for (Recipes r : newList) {
                    recipesList.add(r);
                }
                rvAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Recipes>> call, Throwable t) {
                Log.e(TAG, "failure on call");
            }
        });
        return recipesList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);
        rvRecipes = (RecyclerView) v.findViewById(R.id.rvRecipes);
        rvRecipes.setHasFixedSize(true);

        //Layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        rvRecipes.setLayoutManager(layoutManager);
        recipesList = new ArrayList<>();
        recipesList = loadRecipes();

        //Adapter
        rvAdapter = new ListAdapter(recipesList, getContext(), getActivity());
        rvRecipes.setAdapter(rvAdapter);
        return v;
    }
}
