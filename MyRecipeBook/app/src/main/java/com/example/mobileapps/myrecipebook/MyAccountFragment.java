package com.example.mobileapps.myrecipebook;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobileapps.myrecipebook.constant.Tools;
import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.model.User;
import com.example.mobileapps.myrecipebook.services.RecipeService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyAccountFragment extends Fragment {

    public static String TAG = MyAccountFragment.class.toString();

    User currentUser = LoginActivity.currentUser;
//    UserService userService = Tools.getInitService(UserService.class);
    RecipeService recipeService = Tools.getInitService(RecipeService.class);

    ImageView portrait;
    TextView tvEmail;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<Recipes> recipesList = new ArrayList<>();

    public MyAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        //init component;
        portrait = view.findViewById(R.id.imPortait);
        tvEmail = view.findViewById(R.id.tvEmail);

        //edit image  listener
        portrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        //set recyler view
        recyclerView = view.findViewById(R.id.rvContent);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ListAdapter(recipesList, getContext(), getActivity());
        recyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        //loading image;
        getPortrait();
        //loading email , from current user var;
        tvEmail.setText(currentUser.getEmail());
        //loading recipes;
        getRecipes(currentUser.getId());
    }

    private void getRecipes(int id) {
        Call<List<Recipes>> listRecipse = recipeService.getRecipes(id);
        listRecipse.enqueue(new Callback<List<Recipes>>() {
            @Override
            public void onResponse(Call<List<Recipes>> call, Response<List<Recipes>> response) {
                if (response.isSuccessful()) {
                    List<Recipes> list = response.body();
                    Log.d(TAG, "onResponse: " + list);
                    updateRecipesList(list);
                }else {
                    Log.d(TAG, "get recipes size : 0 ");
                }
            }

            @Override
            public void onFailure(Call<List<Recipes>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateRecipesList(List<Recipes> list) {
        recipesList.clear();
        recipesList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    private void getPortrait() {

    }


}
