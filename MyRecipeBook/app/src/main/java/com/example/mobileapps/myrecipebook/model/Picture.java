package com.example.mobileapps.myrecipebook.model;

import com.google.gson.annotations.SerializedName;

public class Picture {

    @SerializedName("id")
    int id;

    @SerializedName("picture")
    String picture;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
