package com.example.mobileapps.myrecipebook.services;

import android.graphics.Bitmap;

import com.example.mobileapps.myrecipebook.model.Favorites;
import com.example.mobileapps.myrecipebook.model.Ingredient;
import com.example.mobileapps.myrecipebook.model.Picture;
import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.model.Steps;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RecipeService {

    @GET("recipes")
    Call<List<Recipes>> getRecipe();

    @GET("recipes/{id}")
    Call<List<Recipes>> getAllRecipes(@Path("id") int id);

    @GET("ingredients/{recipeid}")
    Call<List<Ingredient>> getIngredients(@Path("recipeid") int recipeid);

    @POST("ingredients")
    Call<Integer> createIngredient(@Body Ingredient ingredient);

    @GET("steps/{recipeid}")
    Call<List<Steps>> getSteps(@Path("recipeid") int recipeid);

    @POST("step")
    Call<Integer> createSteps(@Body Steps steps);

    @POST("recipes")
    Call<Integer> createRecipe(@Body Recipes recipe);

    @GET("{ownerId}/recipes")
    Call<List<Recipes>> getRecipes(@Path("ownerId") int ownerId);

    @GET("favorite/{ownerId}")
    Call<List<Recipes>> getFavorite(@Path("ownerId") int ownerId);

    @POST("addfavorite")
    Call<Integer> addFavorite(@Body Favorites favorites);

    @DELETE("removefavorite/{userId}/{recipeId}")
    Call<Boolean> removeFavorite(@Path("userid") int userId, @Path("recipeId") int recipeId);

    @GET("pictures/{id}")
    Call<Picture> getPicture(@Path("id") int id);

    @POST("uploadpic")
    Call<Integer> uploadPicture(@Body Picture picture);

    @GET("isfavorite/{userId}/{recipeId}")
    Call<Boolean> isFavorite(@Path("userId") int userId, @Path("recipeId")int recipeId);



}
