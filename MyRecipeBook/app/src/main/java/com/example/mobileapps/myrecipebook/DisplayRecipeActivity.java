package com.example.mobileapps.myrecipebook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import android.widget.CompoundButton;

import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobileapps.myrecipebook.constant.Tools;
import com.example.mobileapps.myrecipebook.model.Favorites;
import com.example.mobileapps.myrecipebook.model.Picture;
import com.example.mobileapps.myrecipebook.model.Steps;
import com.example.mobileapps.myrecipebook.model.User;
import com.example.mobileapps.myrecipebook.services.RecipeService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplayRecipeActivity extends AppCompatActivity {

    private static final String TAG = "DisplayRecipeActivity";

    TextView tvSteps;

    int recipeID, pictureId, userID;
    TextView tvName, tvDesc, tvCat, tvIngredients, tvServings, tvPrepTime;
    ImageView ivPicture;

    Switch switchLike;

    private ProgressDialog progressDialog;

    RecipeService recipeService = Tools.getInitService(RecipeService.class);
    User currentUser = LoginActivity.currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_recipe);
        Intent intent = getIntent();
        recipeID = intent.getIntExtra("recipeID", 1);
        String recipeName = intent.getStringExtra("name");
        String recipeDesc = intent.getStringExtra("description");
        String category = intent.getStringExtra("category");
        pictureId = intent.getIntExtra("pictureID", 1);
        String ingredients = intent.getStringExtra("ingredients");
        Log.d(TAG, "ingredients*******" + ingredients);
        int numServings = intent.getIntExtra("numServings", 1);
        int prepTime = intent.getIntExtra("prepTime", 1);

        tvSteps = (TextView) findViewById(R.id.etSteps);
        tvName = (TextView) findViewById(R.id.etName);
        tvDesc = (TextView) findViewById(R.id.etDesc);
        tvCat = (TextView) findViewById(R.id.tvCat);
        tvIngredients = (TextView) findViewById(R.id.etIngredients);
        tvServings = (TextView) findViewById(R.id.tvServings);
        tvPrepTime = (TextView) findViewById(R.id.tvPrepTime);
        ivPicture = (ImageView) findViewById(R.id.ivPicture);

        //Filling the fields
        tvName.setText(recipeName);
        tvDesc.setText(recipeDesc);
        tvCat.setText(category);
        tvServings.setText(String.valueOf(numServings) + " people");
        tvPrepTime.setText(String.valueOf(prepTime) + " minutes");
        tvIngredients.setText(ingredients);

        switchLike = findViewById(R.id.switchLike);
        isFavorite(currentUser.getId(), recipeID);

        switchLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    addItToFavorite();
                } else {
                    removeItFromFavorite();
                }
            }
        });
    }

    private void removeItFromFavorite() {
        Call<Boolean> call = recipeService.removeFavorite(currentUser.getId(), recipeID);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(DisplayRecipeActivity.this, "successed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DisplayRecipeActivity.this, "successed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(DisplayRecipeActivity.this, "called Failed!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addItToFavorite() {

        Favorites favorites = new Favorites();
        favorites.setRecipeId(recipeID);
        favorites.setUserId(currentUser.getId());

        Call<Integer> call = recipeService.addFavorite(favorites);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(DisplayRecipeActivity.this, "Add it Successful!", Toast.LENGTH_SHORT).show();
                } else {
                    switchLike.setChecked(false);
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                switchLike.setChecked(false);
                Toast.makeText(DisplayRecipeActivity.this, "Add it Fail!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void loadSteps() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:7777/api.php/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RecipeService recipeService = retrofit.create(RecipeService.class);

        Call<List<Steps>> call = recipeService.getSteps(recipeID);

        call.enqueue(new Callback<List<Steps>>() {
            @Override
            public void onResponse(Call<List<Steps>> call, Response<List<Steps>> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Code " + response.code());
                }
                List<Steps> newList = response.body();
                Log.v(TAG, "response.body: " + response.body());
                Log.v(TAG, "newlist: " + newList);

                // Toast.makeText(DisplayRecipeActivity.this, "succeed", Toast.LENGTH_LONG).show();

                String stepList = "";
                for (Steps s : newList) {
                    stepList += s.toString();
                }
                tvSteps.setText(stepList);

            }

            @Override
            public void onFailure(Call<List<Steps>> call, Throwable t) {
                Log.e(TAG, "failure on call");
            }
        });
    }

    public void isFavorite(int userID, final int recipeID) {
        RecipeService recipeService = Tools.getInitService(RecipeService.class);
        Call<Boolean> callFavorite = recipeService.isFavorite(userID, recipeID);

        callFavorite.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    switchLike.setChecked(response.body());
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e(TAG, "failure on call to boolean isFavorite");
            }
        });
    }

    public void loadIngredients() {
//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://10.0.2.2:7777/api.php/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
//
//        RecipeService recipeService = retrofit.create(RecipeService.class);
//        Call<List<Ingredient>> ingredientCall = recipeService.getIngredients(recipeID);
//
//        ingredientCall.enqueue(new Callback<List<Ingredient>>() {
//            @Override
//            public void onResponse(Call<List<Ingredient>> call, Response<List<Ingredient>> response) {
//                if (!response.isSuccessful()) {
//                    Log.e(TAG, "Ingredients failed, Code " + response.code());
//                }
//                List<Ingredient> ingredientList = response.body();
//                Log.v(TAG, "INGREDIENTS ****response.body: " + response.body());
//                Log.v(TAG, "INGREDIENTS ****newlist: " + ingredientList);
//
//
//                String ingredients = "";
//                for (Ingredient ingredient : ingredientList) {
//                    ingredients += ingredient.toString();
//                }
//                tvIngredients.setText(ingredients);
//            }
//
//            @Override
//            public void onFailure(Call<List<Ingredient>> call, Throwable t) {
//                Log.e(TAG, "failure on ingredientsCall");
//            }
//        });
    }

    public void loadPicture() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:7777/api.php/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RecipeService recipeService = retrofit.create(RecipeService.class);
        Call<Picture> pictureCall = recipeService.getPicture(pictureId);

        Log.d(TAG, "pictureID ******: " + pictureId);
        pictureCall.enqueue(new Callback<Picture>() {
            @Override
            public void onResponse(Call<Picture> call, Response<Picture> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "picture loading failedCode " + response.code());
                }
                if (response.body() != null) {
                    Picture picture = response.body();
                    Log.e(TAG, "Code " + response.code());
                    byte[] imgAsBytes = Base64.decode(picture.getPicture(), Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(imgAsBytes, 0, imgAsBytes.length);
                    ivPicture.setImageBitmap(bmp);
                    dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<Picture> call, Throwable t) {
                Log.e(TAG, "failure on pictureCall");
            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showProgressDialog();
        loadSteps();
        //loadIngredients();
        loadPicture();
    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(DisplayRecipeActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
        }
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
