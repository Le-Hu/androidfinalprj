package com.example.mobileapps.myrecipebook.services;

import com.example.mobileapps.myrecipebook.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET("user/{id}")
    Call<List<User>> getUserById(@Path("id") int id);

    @GET("user/{email}")
    Call<List<User>> getUserByEmail(@Path("email") String email);

    @POST("register")
    Call<Integer> register(@Body User user);

    @PUT("user/{id}")
    Call<Boolean> updateUserInfo(@Path("id") int id, @Body User user);

}
