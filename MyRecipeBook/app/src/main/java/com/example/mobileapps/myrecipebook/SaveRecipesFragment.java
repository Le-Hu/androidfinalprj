package com.example.mobileapps.myrecipebook;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mobileapps.myrecipebook.constant.Tools;
import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.model.User;
import com.example.mobileapps.myrecipebook.services.RecipeService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SaveRecipesFragment extends Fragment {

    public static String TAG = SaveRecipesFragment.class.toString();
    User currentUser = LoginActivity.currentUser;
    //    UserService userService = Tools.getInitService(UserService.class);
    RecipeService recipeService = Tools.getInitService(RecipeService.class);


    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<Recipes> myDataset = new ArrayList<>();


    public SaveRecipesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_save_recipes, container, false);

        //set recyler view
        recyclerView = view.findViewById(R.id.rvContent);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ListAdapter(myDataset, getContext(), getActivity());
        recyclerView.setAdapter(mAdapter);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getRecipes(currentUser.getId());
    }


    private void getRecipes(int id) {
        Call<List<Recipes>> listRecipe = recipeService.getFavorite(id);
        listRecipe.enqueue(new Callback<List<Recipes>>() {
            @Override
            public void onResponse(Call<List<Recipes>> call, Response<List<Recipes>> response) {
                if (response.isSuccessful()) {
                    List<Recipes> list = response.body();
                    Log.d(TAG, "onResponse: " + list);
                    updateRecipesList(list);
                } else {
                    Log.d(TAG, "get recipes size : 0 ");
                }
            }

            @Override
            public void onFailure(Call<List<Recipes>> call, Throwable t) {
                Log.d(TAG, "onFailure: zzz " + t.getStackTrace(), t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateRecipesList(List<Recipes> list) {
        myDataset.clear();
        myDataset.addAll(list);
        mAdapter.notifyDataSetChanged();

    }

}
