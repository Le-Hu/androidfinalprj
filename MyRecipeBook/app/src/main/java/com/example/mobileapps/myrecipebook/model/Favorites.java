package com.example.mobileapps.myrecipebook.model;

import com.google.gson.annotations.SerializedName;

public class Favorites {

    @SerializedName("id")
    int id;

    @SerializedName("userId")
    int userId;

    @SerializedName("recipeId")
    int recipeId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }
}
