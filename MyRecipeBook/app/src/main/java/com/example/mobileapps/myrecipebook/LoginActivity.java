package com.example.mobileapps.myrecipebook;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
//
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
import com.example.mobileapps.myrecipebook.constant.Constant;
import com.example.mobileapps.myrecipebook.constant.Tools;
import com.example.mobileapps.myrecipebook.model.User;
import com.example.mobileapps.myrecipebook.services.UserService;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = LoginActivity.class.toString();

    UserService userService;

    Button btLogin;
    SignInButton googleSignInButton;
    //    Button signOutBtn;
//    Button disconnectedBtn;
    TextView emaiTextView;
    TextView pwdTextView;


    //google signin
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    //globe static var
    public static User currentUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //init services
//        this.initService();
        userService = Tools.getInitService(UserService.class);

        btLogin = findViewById(R.id.btLogin);
//        btLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(intent);
//            }
//        });

        emaiTextView = findViewById(R.id.etEmail);
        pwdTextView = findViewById(R.id.etPassword);

        // google signin button;
        googleSignInButton = (SignInButton) findViewById(R.id.sign_in_button);

//        signOutBtn = findViewById(R.id.signOutButton);
//        disconnectedBtn = findViewById(R.id.disconnectButton);

        googleSignInButton.setOnClickListener(this);
//        signOutBtn.setOnClickListener(this);
//        disconnectedBtn.setOnClickListener(this);

        //config google sign in
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        //build a google sign in client;
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //build a firebase part to auth ==> firebase obj;
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
        Log.d(TAG, "onStart: current " + currentUser);
    }

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.d(TAG, "Google sign in failed", e);
            }
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, "signInWithCredential:failure", task.getException());
                    //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                    Toast.makeText(LoginActivity.this, "login failed", Toast.LENGTH_LONG).show();
                    updateUI(null);
                }
            }
        });
    }
    // [END auth_with_google]

    // [START signin]
    private void signInByGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
//        FirebaseAuth.getInstance().signOut();
        mAuth.signOut();
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
        Log.d(TAG, "signOut: sign out. done....");
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            Log.d(TAG, "updateUI: login success! " + user.getEmail());
            currentUser = new User();
            currentUser.setEmail(user.getEmail());
            //todo: create new user by restapi ; instert user table;
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            currentUser = null;
            Log.d(TAG, "updateUI: logout c.");
        }
    }

    private void revokeAccess() {
        // Firebase sign out
        mAuth.signOut();
        // Google revoke access
        mGoogleSignInClient.revokeAccess().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                        Log.d(TAG, "onComplete: after revokeaccess user = null");
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.sign_in_button:
                signInByGoogle();
                break;
            case R.id.btLogin:
                signIn();
                break;
            case R.id.btRegister:
                register();
                break;
            default:
                break;
        }
    }

    private void register() {
        final User user = new User();
        user.setEmail(emaiTextView.getText().toString());
        user.setPassword(pwdTextView.getText().toString());
        //todo: deal with username;
        user.setUserName(user.getEmail());

        final Call<Integer> register = userService.register(user);
        register.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                if (response.isSuccessful()) {
                    int id = response.body();
                    if (id == -1) {
                        Toast.makeText(LoginActivity.this, "Email is already used.", Toast.LENGTH_LONG).show();
                        return;
                    }
                    user.setId(id);
                    currentUser = user;
                    Toast.makeText(LoginActivity.this, "User added with id " + id, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(LoginActivity.this, "Register fail :\n" + error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.d(TAG, "onFailure: register action: " + t.getMessage(), t);
                Toast.makeText(LoginActivity.this, t.getMessage() +
                        "check server side", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * sign in by local system;
     */
    private void signIn() {
        String email = emaiTextView.getText().toString();
        final String pwd = pwdTextView.getText().toString();
        Call<List<User>> loginUser = userService.getUserByEmail(email);
        loginUser.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> list = response.body();
                if (list.size() == 0) {
                    Toast.makeText(LoginActivity.this, " Email is wrong!", Toast.LENGTH_LONG).show();
                    return;
                }
                User user = list.get(0);
                if (user.getPassword().equals(pwd)) {
                    Log.d(TAG, "signIn onResponse: "+currentUser);
                    currentUser = user;
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "password is wrong!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

//    private void initService() {
//        Gson gson = new GsonBuilder().setLenient().create();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constant.Base_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
//        userService = retrofit.create(UserService.class);
//    }
}
