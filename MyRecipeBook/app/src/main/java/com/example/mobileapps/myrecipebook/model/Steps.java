package com.example.mobileapps.myrecipebook.model;

import com.google.gson.annotations.SerializedName;

public class Steps {
    @SerializedName("id")
    int id;
    @SerializedName("stepnumber")
    int stepnumber;
    @SerializedName("step")
    String step;

    @SerializedName("recipeID")
    int recipesId;

    public int getRecipesId() {
        return recipesId;
    }

    public void setRecipesId(int recipesId) {
        this.recipesId = recipesId;
    }

    public Steps() {
    }

    public Steps(int id, int stepnumber, String step) {
        this.id = id;
        this.stepnumber = stepnumber;
        this.step = step;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStepnumber(int stepnumber) {
        this.stepnumber = stepnumber;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public int getId() {
        return id;
    }

    public int getStepnumber() {
        return stepnumber;
    }

    public String getStep() {
        return step;
    }

    @Override
    public String toString() {
        return step + "\n";
    }
}
