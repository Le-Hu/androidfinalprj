package com.example.mobileapps.myrecipebook.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Recipes {

    @SerializedName("id")
    private int id;

    @SerializedName("ownerid")
    private int ownerid;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("category")
    private String category;

    @SerializedName("preparationTime")
    private int preparationTime;

    @SerializedName("numberofservings")
    private int numberofservings;

    @SerializedName("pictureid")
    private int pictureId;

    @SerializedName("ingredients")
    private String ingredients;

    public Recipes() {
    }

    public Recipes(int id, int ownerid, String name, String description, String category, int preparationTime, int numberofservings, int pictureId, String ingredients) {
        this.id = id;
        this.ownerid = ownerid;
        this.name = name;
        this.description = description;
        this.category = category;
        this.preparationTime = preparationTime;
        this.numberofservings = numberofservings;
        this.pictureId = pictureId;
        this.ingredients = ingredients;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(int ownerid) {
        this.ownerid = ownerid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    public int getNumberofservings() {
        return numberofservings;
    }

    public void setNumberofservings(int numberofservings) {
        this.numberofservings = numberofservings;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getIngredients() {
        return ingredients;
    }

    @Override
    public String toString() {
        return "Recipes{" +
                "id=" + id +
                ", ownerid=" + ownerid +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", preparationTime=" + preparationTime +
                ", numberofservings=" + numberofservings +
                ", pictureId=" + pictureId +
                ", ingredients='" + ingredients + '\'' +
                '}';
    }
}
