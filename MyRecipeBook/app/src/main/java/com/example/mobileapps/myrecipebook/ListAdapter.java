package com.example.mobileapps.myrecipebook;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mobileapps.myrecipebook.constant.Tools;
import com.example.mobileapps.myrecipebook.model.Picture;
import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.services.RecipeService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private List<Recipes> recipesList;
    private Context context;
    Activity activity;

    ProgressDialog progressDialog;


    private static final String TAG = "ListAdapter";

    public ListAdapter() {
    }

    public ListAdapter(List<Recipes> recipesList, Context context, Activity activity) {
        this.recipesList = recipesList;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recipe_item, viewGroup, false);

        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
        showProgressDialog();

        final Recipes recipe = recipesList.get(position);
        holder.tvFirst.setText(recipe.getName());
        holder.tvSecond.setText(recipe.getDescription());

        RecipeService recipeService = Tools.getInitService(RecipeService.class);

        Call<Picture> pictureCall = recipeService.getPicture(recipe.getPictureId());

        Log.d(TAG, "pictureID ******: " + recipe.getPictureId());
        pictureCall.enqueue(new Callback<Picture>() {

            @Override
            public void onResponse(Call<Picture> call, Response<Picture> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Code " + response.code());
                }
                if (response.body() != null) {
                    Picture picture = response.body();
                    Log.e(TAG, "Code " + response.code());
                    byte[] imgAsBytes = Base64.decode(picture.getPicture(), Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(imgAsBytes, 0, imgAsBytes.length);
                    holder.imItem.setImageBitmap(bmp);
                }
            }

            @Override
            public void onFailure(Call<Picture> call, Throwable t) {
                Log.e(TAG, "failure on pictureCall");
            }

        });

        //onClickListener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DisplayRecipeActivity.class);
                //TODO put extra

                int recipeID = recipe.getId();
                String recipeName = recipe.getName();
                String recipeDesc = recipe.getDescription();
                String category = recipe.getCategory();
                int pictureID = recipe.getPictureId();
                int numServings = recipe.getNumberofservings();
                int prepTime = recipe.getPreparationTime();
                String ingredients = recipe.getIngredients();

                Log.v(TAG, "recipeID ****************" + recipeID);
                intent.putExtra("recipeID", recipeID);
                intent.putExtra("name", recipeName);
                intent.putExtra("description", recipeDesc);
                intent.putExtra("category", category);
                intent.putExtra("pictureID", pictureID);
                intent.putExtra("numServings", numServings);
                intent.putExtra("prepTime", prepTime);
                intent.putExtra("ingredients", ingredients);


                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });
        dismissProgressDialog();

    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
        }
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFirst, tvSecond;
        public ImageView imItem;

        public ListViewHolder(View itemView) {

            super(itemView);
            tvFirst = (TextView) itemView.findViewById(R.id.tvFirstLine);
            tvSecond = (TextView) itemView.findViewById(R.id.tvSecondLine);
            imItem = (ImageView) itemView.findViewById(R.id.ivIcon);

        }

        public void bindView(int position) {
            tvFirst.setText("item 1");
            tvSecond.setText("description");

        }
    }

}