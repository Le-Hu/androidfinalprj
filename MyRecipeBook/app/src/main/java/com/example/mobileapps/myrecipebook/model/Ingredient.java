package com.example.mobileapps.myrecipebook.model;

import com.google.gson.annotations.SerializedName;

public class Ingredient {
    @SerializedName("id")
    int id;
    @SerializedName("ingredients")
    String name;
    @SerializedName("recipeID")
    int recipeID;


    public Ingredient() {
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setRecipeID(int recipeID) {
        this.recipeID = recipeID;
    }

    public Ingredient(int id, String name, int recipeID) {
        this.id = id;
        this.name = name;
        this.recipeID = recipeID;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getRecipeID() {
        return recipeID;
    }

    @Override
    public String toString() {
        return "*** " + name + "\n";
    }
}
