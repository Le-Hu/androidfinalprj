package com.example.mobileapps.myrecipebook;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobileapps.myrecipebook.constant.Tools;
import com.example.mobileapps.myrecipebook.model.Ingredient;
import com.example.mobileapps.myrecipebook.model.Picture;
import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.model.Steps;
import com.example.mobileapps.myrecipebook.model.User;
import com.example.mobileapps.myrecipebook.services.RecipeService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreateRecipeFragment extends Fragment {
    ImageView ivPicture;
    EditText etName, etDesc, etIngredients, etSteps, etCat, etNumServings, etPrepTime;
    Button btnPost;

    private static final String IMAGE_DIRECTORY = "/gallery";
    private static final String TAG = "CreateRecipeFragment";
    private int REQCODE_GALLERY = 1, CAMERA = 2;
    User currentUser = LoginActivity.currentUser;
    RecipeService recipeService = Tools.getInitService(RecipeService.class);

    public CreateRecipeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_recipe, container, false);

        ivPicture = view.findViewById(R.id.imageView);
        ivPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        btnPost = view.findViewById(R.id.btnPost);
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postAction();
            }
        });

        etName = view.findViewById(R.id.etName);
        etDesc = view.findViewById(R.id.etDesc);
        etIngredients = view.findViewById(R.id.etIngredients);
        etSteps = view.findViewById(R.id.etSteps);
        etCat = view.findViewById(R.id.etCat);
        etNumServings = view.findViewById(R.id.etNumServings);
        etPrepTime = view.findViewById(R.id.etPrepTime);


        // Inflate the layout for this fragment
        return view;
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallery();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQCODE_GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == REQCODE_GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Log.d(TAG, "ContentURI: " + contentURI);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity().getBaseContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    ivPicture.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getBaseContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            saveImage(thumbnail);
            Toast.makeText(getActivity().getBaseContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity().getBaseContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


    public void postAction() {
        postPicture();
//        cleanAllFields();
//        backToMainFragment();
    }

    private void postRecipes(int pictureId) {
        final Recipes recipes = new Recipes();
        recipes.setOwnerid(currentUser.getId());
        recipes.setName(etName.getText().toString());
        recipes.setDescription(etDesc.getText().toString());
        recipes.setPictureId(pictureId);
        recipes.setCategory(etCat.getText().toString());

        recipes.setNumberofservings(Integer.parseInt(etNumServings.getText().toString()));
        recipes.setPreparationTime(Integer.parseInt(etPrepTime.getText().toString()));
        recipes.setIngredients(etIngredients.getText().toString());
        // callback recipes

        Call<Integer> rid = recipeService.createRecipe(recipes);
        rid.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    int newRecipeId = response.body();
                    //postIngredients(newRecipeId);
                    postSteps(newRecipeId);
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void postSteps(int id) {
        Steps steps = new Steps();
        steps.setStep(etSteps.getText().toString());
        steps.setRecipesId(id);

        Call<Integer> stepsCall = recipeService.createSteps(steps);
        stepsCall.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {

                    cleanAllFields();
                    backToMainFragment();

                    Log.d(TAG, "onResponse: " + response.body());
                } else {
                    Log.d(TAG, "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        // callback

    }

    private void postIngredients(final int id) {
        Ingredient ingredient = new Ingredient();
        ingredient.setName(etIngredients.getText().toString());
        ingredient.setRecipeID(id);

        Call<Integer> ingredientCall = recipeService.createIngredient(ingredient);
        ingredientCall.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    postSteps(id);
                    Log.d(TAG, "onResponse: " + response.body());
                } else {
                    Log.d(TAG, "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void postPicture() {

        Picture picture = new Picture();
        Bitmap bm = ((BitmapDrawable) ivPicture.getDrawable()).getBitmap();
        picture.setPicture(Tools.encodeTobase64(bm));

        Call<Integer> pId = recipeService.uploadPicture(picture);
        pId.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    int pictureId = response.body();
                    postRecipes(pictureId);
//                    currentPicId = pictureId;
                } else {
                    Toast.makeText(getContext(), "upload picture fail!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void backToMainFragment() {
        FragmentTransaction fragmentTransaction = this.getActivity().getSupportFragmentManager().beginTransaction();
        final HomeFragment homeFragment = new HomeFragment();
        fragmentTransaction.replace(R.id.frame, homeFragment);
        fragmentTransaction.commit();
    }

    private void cleanAllFields() {
        //Remove all the values from the layout
        ivPicture.setImageResource(R.drawable.default_picture);
        etName.setText("");
        etDesc.setText("");
        etIngredients.setText("");
        etSteps.setText("");
    }

}
