package com.example.mobileapps.myrecipebook.constant;

//import com.example.mobileapps.myrecipebook.services.UserService;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.util.Base64;

import com.example.mobileapps.myrecipebook.DisplayRecipeActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Tools {

    public static <T> T getInitService(Class c) {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.Base_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        T t = (T) retrofit.create(c);
        return t;
    }


    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
}
